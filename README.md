![alt text][logo]

[logo]: http://student.sut.ac.th/b5970544/24batch/logo.png "Logo Title Text 2"
# 24Batch V2

##### Thai Open Source ROM that's derived from [LineageOS](https://lineageos.org)
****
### 24Batch is from 
## [SUT #24 Batch](https://www.facebook.com/groups/SUT24/) in Suranaree University of Technology
### ❤❤
****
Getting started
---------------

To get started with Android/24Batch, you'll need to get
familiar with [Repo](https://source.android.com/source/using-repo.html) and [Version Control with Git](https://source.android.com/source/version-control.html).

To initialize your local repository using the LineageOS trees, use a command like this:
```
repo init -u git://github.com/24batchv2/android.git -b lineage-16.0-24
```
Then to sync up:
```
repo sync
```
---
###### Made with Heart ❤ and 🛠
###### [24Batch](http://student.sut.ac.th/b5970544/24batch)